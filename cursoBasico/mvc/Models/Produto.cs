using System.ComponentModel.DataAnnotations;

namespace mvc.Models {
    public class Produto {
        [Required]
        public int Id { get; set; }
        public string Nome { get; set; }
        public int Preco { get; set; }
    }
}
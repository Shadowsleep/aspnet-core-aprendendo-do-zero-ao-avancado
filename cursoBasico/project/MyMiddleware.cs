using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace project
{
    public class MyMiddleware
    {
        private RequestDelegate _next;

        /*atravez do construtor recebe a proxima request */
        public MyMiddleware(RequestDelegate next)
        {
            _next = next;
        }
        /*Invoca a request. tem q ser async e o nome tem q ser Invoke. */
        public async Task Invoke(HttpContext context)
        {
            var logicainicio = "faca sua logica antes de processar o request";
            await _next(context);
            var logicafianal = "apos o request faça a logica pro response";
            await context.Response.WriteAsync($" veja aa aula 7{DateTime.Now}");
        }

    }
}
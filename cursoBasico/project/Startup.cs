using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace project
{
    public class Startup
    {
        private IConfigurationRoot _configuration;
        //public Startup(IConfiguration configuration)
        // {
        //    Configuration = configuration;
        //}
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder().SetBasePath(env.ContentRootPath).AddJsonFile("appConfig.json");

            builder.AddEnvironmentVariables();

            _configuration = builder.Build();

        }
        public void Configure(IApplicationBuilder app)
        {
            app.UseMiddleware<MyMiddleware>();
            app.UseStaticFiles();
            var applicationName = _configuration.GetValue<string>("ApplicationName");
            app.Run(context => context.Response.WriteAsync($"Olá mundo de novo.| {applicationName}"));
        }
    }
}
using Microsoft.EntityFrameworkCore;
using Dominio.Entidades;

namespace Dados
{
    public class ApplicationDBContext : DbContext
    {
        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options) : base(options)
        {

        }
        public DbSet<Categoria> Categorias { get; set; }
        public  DbSet<Produto> Produtos { get; set; }
    }
}